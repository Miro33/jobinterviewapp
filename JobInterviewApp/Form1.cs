﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JobInterviewApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openCSFfile_btn_Click(object sender, EventArgs e)
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                    try
                    {
                        TrajectoryDataSet trajectoryDataSet = new TrajectoryDataSet(filePath, true);
                        trajectory_dgv.DataSource = trajectoryDataSet.CreateDataTable();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("File import failed.\nFile is not CSV or is corrupted!");
                        // log ex to logfile
                    }
                }
            }
        }
    }
}
