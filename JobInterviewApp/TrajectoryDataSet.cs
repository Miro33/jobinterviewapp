﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobInterviewApp
{
    public class TrajectoryDataSet
    {
        private string[] header = { };
        private List<string[]> rowEntries = new List<string[]>();
        private string parsingDelimiter = ",";
        private bool hasHeader;
        public TrajectoryDataSet(string path, bool CSVhasHeader) //default delimiter - comma
        {
            hasHeader = CSVhasHeader;
            ParseCSVFile(path);
        }

        public TrajectoryDataSet(string path, bool CSVhasHeader, string delimiter)
        {
            hasHeader = CSVhasHeader;
            ParseCSVFile(path);
            parsingDelimiter = delimiter;
        }

        private void ParseCSVFile(string path)
        {

            using (TextFieldParser textFieldParser = new TextFieldParser(path))
            {
                bool csvFileHasHeader = hasHeader;

                textFieldParser.TextFieldType = FieldType.Delimited;
                textFieldParser.SetDelimiters(parsingDelimiter);

                while (!textFieldParser.EndOfData)
                {
                    if (csvFileHasHeader)
                    {
                        header = textFieldParser.ReadFields();
                        csvFileHasHeader = false;
                    }
                    else
                    {
                        rowEntries.Add(textFieldParser.ReadFields());
                    }
                }
            }
        }

        public DataTable CreateDataTable()
        {
            DataTable dt = new DataTable();

            if (hasHeader)
            {
                foreach (string columnName in header)
                {
                    dt.Columns.Add(columnName, typeof(string));
                }

                foreach (string[] rowEntry in rowEntries)
                {
                    dt.Rows.Add(rowEntry);
                }

            }
            else
            {

                for (int i = 0; i < rowEntries[0].Length; i++)
                {
                    dt.Columns.Add();
                }

                foreach (string[] rowEntry in rowEntries)
                {
                    dt.Rows.Add(rowEntry);
                }
            }
            return dt;
        }
    }
}
