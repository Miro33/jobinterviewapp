﻿namespace JobInterviewApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openCSFfile_btn = new System.Windows.Forms.Button();
            this.trajectory_dgv = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.trajectory_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // openCSFfile_btn
            // 
            this.openCSFfile_btn.Location = new System.Drawing.Point(12, 12);
            this.openCSFfile_btn.Name = "openCSFfile_btn";
            this.openCSFfile_btn.Size = new System.Drawing.Size(75, 23);
            this.openCSFfile_btn.TabIndex = 0;
            this.openCSFfile_btn.Text = "Open CSV File";
            this.openCSFfile_btn.UseVisualStyleBackColor = true;
            this.openCSFfile_btn.Click += new System.EventHandler(this.openCSFfile_btn_Click);
            // 
            // trajectory_dgv
            // 
            this.trajectory_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.trajectory_dgv.Location = new System.Drawing.Point(12, 41);
            this.trajectory_dgv.Name = "trajectory_dgv";
            this.trajectory_dgv.Size = new System.Drawing.Size(776, 397);
            this.trajectory_dgv.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.trajectory_dgv);
            this.Controls.Add(this.openCSFfile_btn);
            this.Name = "Form1";
            this.Text = "CSV Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.trajectory_dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button openCSFfile_btn;
        private System.Windows.Forms.DataGridView trajectory_dgv;
    }
}

